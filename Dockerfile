FROM ubuntu

LABEL maintainer="Musker.Chao <https://gitee.com/spdir/whois-query.git>"

RUN apt-get update && \
    apt-get -y install python3 python3-pip python3-dev && \
    apt-get clean

WORKDIR /opt/whois-query

COPY . .

RUN python3 -m pip install -r requirements.txt -i https://pypi.doubanio.com/simple && \
    chmod +x start.sh

EXPOSE 8080

CMD ["/opt/whois-query/start.sh"]
