#!/usr/bin/env bash
ROOT_DIR=${0%/*}
WHOIS_MAIN=${ROOT_DIR}/main.py
whatis python3
if [[ $? != 0 ]]; then
    python3 ${WHOIS_MAIN}
else
  pyVersion=`python -V 2>&1|awk '{print $2}'|awk -F '.' '{print $1}'`
  if [ $pyVersion == "3" ]; then
    python ${WHOIS_MAIN}
  else
    echo "Please use python3 to run whois-query!!!"
  fi
fi