# Whois-query

---

### 项目介绍

免费Whois查询接口，完全开放

1. API接口,返回JSON格式数据(支持POST,GET方式)
2. 网页查询接口(支持POST,GET方式)
3. 支持上千种域名查询

#### 测试接口

1. 页面: [http://whois.devopsclub.cn](http://whois.devopsclub.cn)

2. Api: [http://whois.devopsclub.cn/whoisapi?domain_name=](http://whois.devopsclub.cn/whoisapi?domain_name=baidu.com)

```json
{
    "code": "1",
    "data": {
        "DomainName": "BAIDU.COM",
        "RegistryDomainID": "11181110_DOMAIN_COM-VRSN",
        "RegistrarWhoisServer": "whois.markmonitor.com",
        "RegistrarUrl": "http://www.markmonitor.com",
        "UpdatedDate": "2019-05-09T04:30:46Z",
        "CreationDate": "1999-10-11T11:05:17Z",
        "RegistryExpiryDate": "2026-10-11T11:05:17Z",
        "Registrar": "MarkMonitor Inc.",
        "RegistrarIanaId": "292",
        "RegistrarAbuseContactEmail": "abusecomplaints@markmonitor.com",
        "RegistrarAbuseContactPhone": "+1.2083895740",
        "DomainStatus": [
            "clientDeleteProhibited https://icann.org/epp#clientDeleteProhibited",
            "clientTransferProhibited https://icann.org/epp#clientTransferProhibited",
            "clientUpdateProhibited https://icann.org/epp#clientUpdateProhibited",
            "serverDeleteProhibited https://icann.org/epp#serverDeleteProhibited",
            "serverTransferProhibited https://icann.org/epp#serverTransferProhibited",
            "serverUpdateProhibited https://icann.org/epp#serverUpdateProhibited"
        ],
        "NameServer": [
            "NS1.BAIDU.COM",
            "NS2.BAIDU.COM",
            "NS3.BAIDU.COM",
            "NS4.BAIDU.COM",
            "NS7.BAIDU.COM"
        ]
    }
}
```

  ```txt
  返回状态码:
    1)  1  域名信息获取成功
    2)  0  域名不存在
    3) -1  暂不支持此域名查询
    4) -2  域名输入有误
    5) -3  域名查询失败
    6) -4  服务器异常
  ```

#### 运行服务端

**一、直接运行主程序文件启动服务**

直接运行main.py文件，需要python 3.5+版本

1、 安装依赖包

  ```bash
    python3 -m pip install -r requirements.txt
  ```

2、运行服务端程序

  ```bash
    python3 main.py
    运行时可以指定的参数
    `--port`: 指定程序运行的端口
  ```

**二、使用Docker运行服务端**

```bash
docker run -d --name=whois-query -p 8080:8080 registry.cn-beijing.aliyuncs.com/musker/whois-query
```
